<?php

namespace App\Tests;

use App\Entity\Product;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testSomething(): void
    {
        $demo = new Product();
        $demo->setName('p1');
        $this->assertTrue($demo->getName() === 'p1');
    }
}
