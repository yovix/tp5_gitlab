<?php

namespace App\Tests;

use App\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class FunctionalTest extends WebTestCase
{
    public function testDisplayProductIndex(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/product');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Product index');
    }

    public function testDisplayCreateProduct(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/product/new');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Create new Product');
    }

    public function testCreateProduct(): void
    {
        $client = static::createClient();
        $client->followRedirects();

        $crawler = $client->request('GET', '/product/new');

        $btn = $crawler->selectButton("Save");
        $form = $btn->form([
            'product[name]' => 'product' .uniqid(),
            'product[price]' => random_int(1,100),
        ]);

        $client->submit($form);
        
        $this->assertResponseIsSuccessful();

        $name = $form['product[name]']->getValue();
        $price = $form['product[price]']->getValue();

        $this->assertSelectorTextContains('body', $price);
    }
}
